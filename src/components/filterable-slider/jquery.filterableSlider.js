;(function ($, window) {
    'use strict';

    $.plugin('wemFilteredSlider', {
        defaults: { },
        init: function() {
            var me = this;

            me.applyDataAttributes();

            var filtersContainer = document.getElementById("client-filters");
            var buttons = filtersContainer.getElementsByClassName("btn");

            for (var i = 0; i < buttons.length; i++) {
                buttons[i].addEventListener("click", function() {
                    var current = filtersContainer.getElementsByClassName("btn-outline-primary");
                    current[0].className = current[0].className.replace(" btn-outline-primary", " btn-link");
                    this.className.replace(" btn-link", "");
                    this.className += " btn-outline-primary";
                    me.filterClients($(this).data("filter-category"));
                });
            }
        },

        filterClients: function(category) {
            if (category === "no-filter") {
                $("#clients .client").addClass("show");
            } else {
                $("#clients > .show").removeClass("show");
                $("#clients .client[data-category=" + category + "]").addClass("show");
            }
        },

        destroy: function() {
            var me = this;

            me._destroy();
        }
    });
})(jQuery, window);;
