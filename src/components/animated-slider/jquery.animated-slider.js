;(function ($, window) {
  'use strict';

  $.plugin('wemAnimatedSlider', {
    defaults: {
      slideContentSelector: '[data-controller="animated-slider/slide/content"]',
      cssClassAnimated: 'animated fadeIn',
      slickOptions: {
        accessibility: true,
        adaptiveHeight: false,
        arrows: false,
        asNavFor: null,
        autoplay: false,
        autoplaySpeed: 3000,
        centerMode: false,
        centerPadding: '50px',
        cssEase: 'ease',
        dots: true,
        dotsClass: 'animated-slider__dots',
        draggable: true,
        easing: 'linear',
        edgeFriction: 0.35,
        focusOnSelect: false,
        infinite: false,
        initialSlide: 0,
        mobileFirst: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        pauseOnDotsHover: false,
        respondTo: 'window',
        responsive: null,
        rows: 1,
        rtl: false,
        slide: '',
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        swipe: true,
        swipeToSlide: false,
        touchMove: true,
        touchThreshold: 5,
        useCSS: true,
        useTransform: true,
        variableWidth: false,
        vertical: false,
        verticalSwiping: false,
        waitForAnimate: true,
        fade: false,
        mode: 'local'
      },
    },

    /**
     * Inits the plugin.
     */
    init: function () {
      const me = this

      me.registerEvents();

      me.$el.slick(me.opts.slickOptions);

    },
    /**
     * Registers all events.
     */
    registerEvents: function () {
      var me = this;

      var debounce = function (func, delay) {
        let inDebounce
        return function () {
          const context = this
          const args = arguments
          clearTimeout(inDebounce)
          inDebounce = setTimeout(function () {
            func.apply(context, args)
          }, delay)
        }
      }

      me._on(me.$el, 'init', $.proxy(me.onInit, me));
      me._on(me.$el, 'lazyLoaded', $.proxy(me.onLazyLoaded, me));
      me._on(me.$el, 'beforeChange', $.proxy(me.onBeforeChange, me));
    },

    onLazyLoaded (evt, slick, $img ) {
      var me = this
      $img
        // Find the parent <picture> tag of img
        .closest('picture')
        // Find <source> tags with data-lazy-srcset attribute
        .find('source[data-lazy-srcset]')
        // Copy data-lazy-srcset to srcset
        .each(function (i, source) {
          var $source = $(source);
          $source.attr('srcset', $source.data('lazy-srcset'));
        });
    },

    onInit () {
      var me = this
      if (me.$el.find('.slick-slide').hasClass('slick-active')) {
        me.$el.find(me.opts.slideContentSelector).addClass(me.opts.cssClassAnimated);
      } else {
        me.$el.find(me.opts.slideContentSelector).removeClass(me.opts.cssClassAnimated);
      }
    },

    onBeforeChange (t, slick, currentSlide, nextSlide) {
      var me = this

      if (currentSlide === nextSlide) {
        return;
      }

      $(slick.$slides[nextSlide]).find(me.opts.slideContentSelector).removeClass(me.opts.cssClassAnimated).hide();

      $(slick.$slides[currentSlide]).find(me.opts.slideContentSelector).removeClass(me.opts.cssClassAnimated).hide();
      setTimeout(() => {
        $(slick.$slides[nextSlide]).find(me.opts.slideContentSelector).addClass(me.opts.cssClassAnimated).show();
      }, 100);
    }
  });
})(jQuery, window);
