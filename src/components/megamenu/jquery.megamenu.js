;(function ($, window) {
  'use strict';

  $.plugin('wemMegamenu', {
    mobileInitialized: false,
    defaults: {
      containerSelector: '[data-megamenu="true"]',
      menuSelector: '[data-controller="megamenu/menu"]',
      menuItemSelector: '[data-controller="megamenu/item"]',
      megaMenuDropdownSelector: '[data-controller="megamenu/dropdown"]',
      megaMenuDropdownLinkListSelector: '[data-controller="megamenu/dropdown/linklist"]',
      toggleMobileSelector: '[data-controller="megamenu/toggleMenu"]',
      cssClassHasDropdown: 'megamenu__item--dropdown',
      cssClassSticky: 'megamenu--sticky',
      mobileMenuOpenedBodyClass: 'mm-ocd-opened'
    },
    /**
     * Inits the plugin.
     */
    init: function () {
      var me = this;

      me.applyDataAttributes();

      me.registerEvents();

      me.$el.find(this.opts.menuItemSelector).each(function (i, el) {
        var $menuItem = $(this)
        if ($menuItem.find(me.opts.megaMenuDropdownSelector).length !== 0) {
          $menuItem.addClass(me.opts.cssClassHasDropdown)
        }
      })

      this.$wrapper = $('<div class="megamenu__wrapper"></div>');
      this.$el.before(this.$wrapper);

      var _min = this.$el.outerHeight();
      this.opts.unpin = Math.max(_min, this.opts.unpin || 0);
      this.opts.pin = Math.max(_min, this.opts.pin || 0);

      $(window).trigger('resize');
    },

    /**
     * init the mobile menu
     */
    initMobileMenu: function () {
      var me = this
      if (me.mobileInitialized) {
        return;
      }

      var $mobileMenu = $('<nav id="megamenu-mobile"></nav>');

      $('body').append($mobileMenu);

      var $baseUl = $('<ul></ul>');

      $mobileMenu.append($baseUl);

      $(me.opts.menuSelector).find(me.opts.menuItemSelector).each(function () {
        var $mainMenuItem = $(this);
        var $li = $('<li></li>');
        var $a = $mainMenuItem.find('> a').clone();
        $li.append($a);

        var $linkList = $mainMenuItem.find(me.opts.megaMenuDropdownSelector).find(me.opts.megaMenuDropdownLinkListSelector);

        if ($linkList.length > 0) {
          var $ul = $('<ul></ul>');
          $linkList.each(function () {

            if ($(this).data('title')) {
              $ul.append('<li class="megamenu__mobile__title"><span>' + $(this).data('title') + '</span></li>');
            }

            var $subUl = null;
            var $lastLi = null;

            $(this).find('> a').each(function () {
              var $li = $('<li></li>');
              $li.append($(this).clone());

              if ($(this).hasClass('megamenu__sublist')) {
                if ($subUl === null) {
                  $subUl = $('<ul></ul>');
                  $lastLi.append($subUl);
                  $subUl.append($li);
                }
                else {
                  $subUl.append($li);
                }
              } else {
                if ($subUl !== null) {
                  $subUl = null;
                }
                $ul.append($li);
              }

              $lastLi = $li;

            })
          });
          $li.append($ul);
        }
        $baseUl.append($li);
      });

      var menu = new MmenuLight(
        document.querySelector("#megamenu-mobile")
      );

      this.mobileMenu = menu;
      this.mobileMenuNavigator = menu.navigation({
        title: 'iPoint',
        theme: 'dark'
      });

      this.mobileMenuDrawer = menu.offcanvas({
        position: 'right'
      });

      $('.mm-ocd__backdrop').on('click touch', function () {
        $(me.opts.toggleMobileSelector).removeClass('is-active');
      });

      me.mobileInitialized = true;
    },

    /**
     * Registers all events.
     */
    registerEvents: function () {
      var me = this;

      var debounce = function (func, delay) {
        let inDebounce
        return function () {
          const context = this
          const args = arguments
          clearTimeout(inDebounce)
          inDebounce = setTimeout(function () {
            func.apply(context, args)
          }, delay)
        }
      }

      me._on($(me.opts.toggleMobileSelector), 'click touch', $.proxy(me.toggleMobileMenu, me))
      me._on($(window), 'scroll resize', debounce($.proxy(me.onScroll, me), 0));

      StateManager.on('resize', function () {
        me.onScroll();
      });

      StateManager.registerListener([
        { state: 'xs', enter: function () { me.initMobileMenu() ;} },
        { state: 'sm', enter: function () { me.initMobileMenu() ;} },
        { state: 'md', enter: function () { me.initMobileMenu() ;} }
      ]);
    },

    toggleMobileMenu: function () {
      var me = this;
      if ($('body').hasClass(me.opts.mobileMenuOpenedBodyClass)) {
        me.mobileMenuDrawer.close();
        $(me.opts.toggleMobileSelector).removeClass('is-active');
      }
      else {
        me.mobileMenuDrawer.open();
        $(me.opts.toggleMobileSelector).addClass('is-active');
      }
    },

    onScroll: function () {
      const me = this;

      var height = this.$el.outerHeight(),
        top = this.$wrapper.offset().top;

      if ($(window).scrollTop() > (top + 40)) {
        this.$wrapper.height(height);
        this.$el.addClass(this.opts.cssClassSticky);
      }
      else {
        this.$el.removeClass(this.opts.cssClassSticky);
        this.$wrapper.height('auto');
      }
    }
  });
})(jQuery, window);
