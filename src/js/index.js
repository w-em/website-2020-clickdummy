(function($, window) {

  // svg for everybody
  svg4everybody();

  window.StateManager.init([
    {
      state: 'xs',
      enter: 0,
      exit: 575
    },
    {
      state: 'sm',
      enter: 576,
      exit: 767
    },
    {
      state: 'md',
      enter: 768,
      exit: 991
    },
    {
      state: 'lg',
      enter: 992,
      exit: 1199
    },
    {
      state: 'xl',
      enter: 1200,
      exit: 99999999
    }
  ]);

  // picture loading
  window.StateManager.addPlugin('[data-slider]', 'wemSlider');
  window.StateManager.addPlugin('[data-counter]', 'wemCounter');
  window.StateManager.addPlugin('[data-picture]', 'wemPicture');
  window.StateManager.addPlugin('[data-megamenu="true"]', 'wemMegamenu');
  window.StateManager.addPlugin('[data-animated-slider="true"]', 'wemAnimatedSlider');
  window.StateManager.addPlugin('[data-filtered-slider="true"]', 'wemFilteredSlider');

})(jQuery, window);
